# Afritek DevOps Channel

Afritek DevOps Channel

Criar um canal (AngoDevOps, Afritek, AfriDevOps) no youtube e ensinar DevOps para o publico angolano/africa

	- Comprar um tripe para o telefone (Me gravar e gravar a tela) -  Tentar fazer o desafio das 6h da manhã para gravar os videos
	- Escolher o editor de videos
	- Criar a marca: Logotipo e Banner para o youtube

	Olá pessoal, bem vindo ao canal AfriDevOps este é um canal onde estarei compartilhando conhecimentos voltados para area de T.I, teremos três priciapais playlist entre elas Virtualização, Computação em nuvem e Desenvolvimento. Em virtualização estarei abordando sobre as principias ferramentas de virtualização e colocaremos a mão na massa também subindo algumas VM´s (Ubuntu desktop, Ubuntu server e Windows 10) e fazer algumas configurações para os principias ambientes de desenvolvimento e produção (Apache, Nginx, MySQL, phpMyAdmin, Samba). Já em computação em nuvem, estarei falando dos três principais players do mercado marcado Azure, GCP e AWS. E para isso, os conceitos de virtualização são muito importante pois a computação em nuvem é resumidamente a utilização de virtualização usando computadores alocados em outras partes do mundo. Essas VM´s dependendo do provedor de nuvem podem ser chamados de instancias como é o caso do GCP e a AWS, também desenvolveremos assuntos como Clusters, Containers, Orquestradores, Backups, Segurança, Monitoramento, Automação de Infraestrutura (IaC) assim como bancos de dados e conexão com aplicações Web e Mobile. E por fim, teremos a playlist voltado para desenvolvimento onde escolheremos uma linguagem de programação frontend e uma backend para desenvolvermos juntos uma aplicação que utilizará todos os recursos ou tecnologias estadadas nesse canal. Os videos serão curtos e objetivos entre 10 a 15min.

	- Virtualização
		- Hyper-V
			- Linux
			- Apache/nginx
			- Samba
			- Zabbix
			- PfSense
			- Grafana
		- VirtualBox

	- Computação em nuvem
		- Azure
			- Virtual machines
			- Clusters | k8s | containers | data-bases (pg, mysql) - conexão (php, nodejs)
				- yaml | helm | deployment | insomnia | postman 
			- Redes
			- Ingress (API Gateway) Nginx | Istio | Kong | Ambassador | Gloo
			- Monitoramento
			- Segurança (API Gateway) Nginx | Istio | Kong | Ambassador | Gloo
			- Backup | Velero
		- GCP
		- AWS
		- IaC | Terraform, Ansible

	- Desenvolvedomento
		- php | html | css
		- javacript
		- nodejs
		- python
		- shellscripts
		- golang
